REBAR=$(shell which rebar3 || echo ./rebar3)

all:
	$(REBAR) compile

run:
	$(REBAR) shell

publish:
	$(REBAR) hex publish
