# Erlang Pipeline Parse Transform

## What is it?

Sometimes erlang programmer wants to apply function composition.
Usually it is done by ugly combining functions, e.g.

    Result = fun3(mod2:fun2(fun1(Arg1, Arg2))).

Other way to do that is creating multiple bindings:

    R1 = fun1(Arg1, Arg2),
    R2 = mod2:fun2(R1),
    Result = fun3(R2).

This parse transform allows the user to write like this:

    Result = [fun1, mod2:fun2, fun3] (Arg1, Arg2).

More complicated options are available -- see `src/pipeline_demo.erl` for examples.


## Including in your applications

### rebar3

Add the following in your dependencies

```erlang
{deps, [
    ...
    {pipeline, {pkg, erl_pipeline}}   %% <--- That's line to add
]}.
```

You want to make sure you specify it that way, with the application being called `pipeline` and the package being called `erl_pipeline`.

### rebar2

If you're still on rebar2 (trust me, I get it, no judgment here, I totally get it), add this to your deps:

```erlang
{deps, [
    {pipeline, {git, "https://github.com/choptastic/pipeline", {branch, master}}}
]}.
```

## Try it out

To use pipeline, simply compile your modules with `-compile({parse_transform, pipeline}).` module attribute.

## License and Team

Creator: [Danil Zogoskin](https://github.com/stolen)
Maintainer: [Jesse Gumm](http://jessegumm.com)
Licenced: [MIT License](https://github.com/choptastic/pipeline/blob/master/LICENSE)

# CHANGES

## 0.2.0

* Updated to work with rebar3 and hex
* Fix compilation issue on Erlang 24
